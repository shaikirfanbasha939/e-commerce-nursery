import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './Header';
import { BrowserRouter,Routes,Route } from 'react-router-dom';
import Products from './Components/Products';
import Blogs from './Components/Blogs';
import Pricing from './Components/Pricing';
import Nopage from './Components/Nopage';
import Home from './Components/Home';

function App() {
  return (
 <>
  <div><Header/></div>
 
  
   <div style={{marginTop:"5rem"}}>
   <BrowserRouter>
      <Routes>
          <Route path="/"  element={<Home />}/>
          <Route path="products" element={<Products/>} />
          <Route path="blogs" element={<Blogs />} />
          <Route path="pricing" element={<Pricing />} />
          <Route path="*" element={<Nopage />} />
      
      </Routes>
    </BrowserRouter>
   </div>
   </>
  );
}

export default App;
