import React from 'react'

function Products() {
  return (
    <div style={{width:500,height:1000,marginLeft:400,marginTop:50}}>
      <h1>Scaling</h1>
      <p>Applications experience variable load. You can handle increased load by upgrading your service to have more resources or by scaling it to have up to 100 instances.</p><br/>
      <p>Render runs a load balancer in front of scaled service instances to evenly distribute network request traffic and supports the following scaling methods:</p><br/>
      <ul>
        <li>Manual scaling: Scale your service to a specific number of instances.</li>
        <li>Autoscaling: Automatically scale your service based on target CPU and memory utilization.</li>
      </ul>
      <br/>
      <h1>Manual scaling</h1>
      <p>Manual scaling is the simplest way to scale out your service. Your service will consistently have the desired number of instances. Open the Scaling tab in your service dashboard, enter the desired number of instances in the Manual Scaling section, and click Save Changes. Your service will immediately start to scale.</p>
    </div>
  )
}

export default Products